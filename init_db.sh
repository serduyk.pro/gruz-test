
#!/bin/bash

# Загрузка переменных окружения из файла .env
set -o allexport
source .env
set +o allexport

# Проверяем, указан ли путь к базе данных
if [ -z "$DATABASE_URI" ]; then
    echo "Error: DATABASE_URI is not specified in the .env file."
    exit 1
fi

# Получаем абсолютный путь к базе данных
database_path="${DATABASE_URI#sqlite://}"
database_absolute_path="$(pwd)/$database_path"

# Проверяем, существует ли база данных по указанному пути
if [ -e "$database_absolute_path" ]; then
    echo "Error: Database already exists at $database_absolute_path"
    exit 1
fi

# Создаем пустой файл базы данных SQLite
mkdir -p "$(dirname "$database_absolute_path")"
touch "$database_absolute_path"

# Уведомляем пользователя об успешном создании базы данных
echo "Database created successfully at $database_absolute_path"
