# Dockerfile
# Используем базовый образ Python
FROM python:3.12.3-bookworm
# Установка зависимостей
COPY requirements.txt /app/requirements.txt
WORKDIR /app
VOLUME /app/instance
RUN pip install --no-cache-dir -r requirements.txt

# Копирование исходного кода приложения в контейнер
COPY . /app

# Указываем переменную окружения для Flask, чтобы сообщить Flask, что мы работаем в режиме production
ENV FLASK_ENV=production

# Определение порта, на котором будет работать приложение Flask
EXPOSE 5000

# Команда для запуска приложения Flask
CMD ["flask", "run", "--host=0.0.0.0"]