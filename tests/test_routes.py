# tests/test_routes.py

import pytest
import sys
from os.path import abspath, dirname
sys.path.append(dirname(dirname(abspath(__file__))))

from app import create_app

@pytest.fixture
def app():
    app = create_app()
    return app

@pytest.fixture
def client(app):
    return app.test_client()

def test_index_page(client):
    response = client.get('/')
    assert response.status_code == 200

def test_about_page(client):
    response = client.get('/about')
    assert response.status_code == 200